﻿using System;

namespace SimulatedDevice
{
	public class SensorReading
	{
		public Guid ReadingId { get; set; }

		public string DeviceId { get; set; }

		public double Value { get; set; }

		public SensorType SensorType { get; set; }

		public DateTime Created { get; set; }
	}
}
