﻿using System;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SimulatedDevice
{
	class Program
	{
		private const int DeviceCount = 50;

		private const string DevicePrefix = "simulated-pi";
		
		private static DeviceClient[] DeviceClients = new DeviceClient[DeviceCount];

		private static Lazy<string> LazyIotHubHost = new Lazy<string>(() => ConfigurationManager.AppSettings["iothub-host"]);

		static void Main()
		{
			JsonConvert.DefaultSettings = () =>
										{
											JsonSerializerSettings settings = new JsonSerializerSettings();
											settings.Converters.Add(new StringEnumConverter());

											return settings;
										};

			Console.WriteLine("***********************************************************");
			Console.WriteLine("*            CLOUDGATE SENSOR DATA SIMULATOR              *");
			Console.WriteLine("*                                                         *");
			Console.WriteLine("***********************************************************");
			Console.WriteLine();
			Console.WriteLine("Press Enter to start the generator.");
			Console.WriteLine("Press Ctrl-C to stop the generator.");
			Console.WriteLine();
			Console.ReadLine();
			Console.WriteLine("Working....");

			SimulateDeviceMessages();

			Console.ReadLine();
		}

		private static async void SimulateDeviceMessages()
		{
			Random random = new Random();
			ConsoleSpinner spinner = new ConsoleSpinner();

			while (true)
			{
				spinner.Spin();

				try
				{
					int id = random.Next(0, DeviceCount);

					string deviceId = $"{DevicePrefix}-{(id + 1):D2}";

					SensorReading speedReading = new SensorReading
												{
													ReadingId = Guid.NewGuid(),
													DeviceId = deviceId,
													SensorType = SensorType.Speed,
													Value = random.NextDouble() * 90,
													Created = DateTime.UtcNow
												};

					SensorReading temperatureReading = new SensorReading
														{
															ReadingId = Guid.NewGuid(),
															DeviceId = deviceId,
															SensorType = SensorType.Temperature,
															Value = (random.NextDouble() * 20) + 50,
															Created = DateTime.UtcNow
														};

					DeviceClient deviceClient = GetDeviceClient(id, deviceId);
					
					deviceClient.SendEventBatchAsync(new[] { speedReading.ToMessage(), temperatureReading.ToMessage() }).Wait();
				}
				catch (Exception exception)
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine("{0} > Exception: {1}", DateTime.Now, exception.Message);
					Console.ResetColor();
				}

				Thread.Sleep(500);
			}
		}

		private static DeviceClient GetDeviceClient(int id, string deviceId)
		{
			if (DeviceClients[id] == null)
			{
				Task<string> task = GetDeviceKeyAsync(deviceId);
				task.Wait();

				DeviceClients[id] = DeviceClient.Create(LazyIotHubHost.Value, new DeviceAuthenticationWithRegistrySymmetricKey(deviceId, task.Result));
			}

			return DeviceClients[id];
		}

		private static async Task<string> GetDeviceKeyAsync(string deviceId)
		{
			RegistryManager registryManager = RegistryManager.CreateFromConnectionString(ConfigurationManager.ConnectionStrings["iothub-owner"].ConnectionString);

			Device device = await registryManager.GetDeviceAsync(deviceId);

			Console.WriteLine("Device key for device [{0}] = {1}", deviceId, device.Authentication.SymmetricKey.PrimaryKey);

			return device.Authentication.SymmetricKey.PrimaryKey;
		}
	}
}
