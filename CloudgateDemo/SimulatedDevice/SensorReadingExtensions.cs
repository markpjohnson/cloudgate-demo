﻿using System.Text;
using Microsoft.Azure.Devices.Client;
using Newtonsoft.Json;

namespace SimulatedDevice
{
	public static class SensorReadingExtensions
	{
		public static Message ToMessage(this SensorReading reading)
		{
			return new Message(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(reading)));
		}
	}
}
