﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;

namespace CloudgateDemo
{
	class Program
	{
		private const int DeviceCount = 50;

		private const string DevicePrefix = "simulated-pi";

		static void Main()
		{
			string connectionString = ConfigurationManager.ConnectionStrings["iothub"].ConnectionString;

			RegistryManager registryManager = RegistryManager.CreateFromConnectionString(connectionString);

			bool addDevices = true;
			if (addDevices)
			{
				for (int i = 0; i < DeviceCount; i++)
				{
					string deviceId = $"{DevicePrefix}-{(i + 1):D2}";
					Console.WriteLine($"Creating device id [{deviceId}]");
					
					AddDeviceAsync(registryManager, deviceId).Wait();
				}
			}
			else
			{
				DeleteAllDevices(registryManager).Wait();
			}
		}

		private static async Task DeleteAllDevices(RegistryManager registryManager)
		{
			List<Device> devices;

			do
			{
				devices = (await registryManager.GetDevicesAsync(DeviceCount)).ToList();
				if (devices.Any())
				{
					await registryManager.RemoveDevicesAsync(devices);
				}
			} while (devices.Any());
			
			Console.WriteLine("All devices removed");
		}

		private static async Task AddDeviceAsync(RegistryManager registryManager, string deviceId)
		{
			Device device;
			try
			{
				device = await registryManager.AddDeviceAsync(new Device(deviceId));
			}
			catch (DeviceAlreadyExistsException)
			{
				device = await registryManager.GetDeviceAsync(deviceId);
			}

			Console.WriteLine("Device key for device [{0}] = {1}", deviceId, device.Authentication.SymmetricKey.PrimaryKey);
		}
	}
}
